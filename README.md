# Search pannel

## Features:
1. Search by title: user can type something and app will find propper tours. The feature implemented with debounce, because it helps reduce many requests.
      
2. Sort by title: helps to sort tours in case if user knows execly city or other information.

3. Sort by special offers: helps to find tours with
promotions.

4. Sort by price: helps to find tours with range of price.

5. Sort by rating: helps to sort tours by rating.

6. Clear all: helps to reset search panel data.

7. Routing for different pages. In the current task there
are two pages: Main and Search

8. I also added images for tours because I belive everything what you do
must be nice and beautiful, especially for FrontEnd development.
      
## Technical scope:
    
1. React - because it is simple and powerful JavaScript lib for building user interfaces.

2. Redux - because it helps to manage ever-changing state easily.

3. React-Router - because it helps handle Sing Page Applications.

4. Webpack - because it is a perfect bundler for many things :)

5. Flexbox for views - because it is a good approach for creating page layouts.

6. Eslint with airbnb rules - because all devs should follow the same rules!

7. Components - because it is more maintainable and other devs easily can continue work on it.
      

## Running (Node v6.11.5 required)

```
# Install node modules
yarn
```


```
# Start dev server on localhost:8080
yarn start
```


```
# Build dev version
yarn dev
//if you want to test it use simple server such as python:
cd dist && python -m SimpleHTTPServer 8000
```


```
# Build prod version
yarn build
# if you want to test it use some simple server such as python:
cd dist && python -m SimpleHTTPServer 8000
```
