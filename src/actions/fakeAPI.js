import fakeTours from './fakeData.json';

// fake BE part to get proper sorted data
const getFakeTours = ({
  search,
  isSorted,
  isSpecialOffer,
  currentPrice,
  currentRating
}) => {
  const { tours } = fakeTours;
  let data = [...tours];

  if (search) {
    data = data.filter(
      tour => tour.title.toLowerCase().indexOf(search.toLowerCase()) !== -1
    );
  }

  if (isSorted) {
    data = [
      ...data.sort((a, b) => {
        if (isSorted) {
          if (a.title < b.title) return -1;
          if (a.title > b.title) return 1;
        }
        return 0;
      })
    ];
  }

  if (isSpecialOffer) {
    data = data.filter(tour => tour.isSpecialOffer === true);
  }

  if (currentPrice) {
    const arr = currentPrice.split(':');
    const min = Number(arr[0]);
    const max = Number(arr[1]);
    data = data.filter(tour => tour.price >= min && tour.price <= max);
  }

  if (currentRating) {
    data = data.filter(tour => tour.rating >= currentRating);
  }

  return { tours: data };
};

export default getFakeTours;
