// we need axios for real API, for this task BE part handles by getFakeTours function
// import axios from 'axios';
import { toastr } from 'react-redux-toastr';
import * as types from '../constants/search';
import getFakeTours from './fakeAPI';

const startLoading = () => ({ type: types.START_LOADING });
const stopLoading = () => ({ type: types.STOP_LOADING });
const addTours = tours => ({ type: types.ADD_TOURS, tours });
const setSort = () => ({ type: types.SET_SORTING });
const setSpecialOffer = () => ({ type: types.SET_SPECIAL_OFFER });
const setSearch = value => ({ type: types.SET_SEARCH, value });
const setPrice = value => ({ type: types.SET_PRICE, value });
const setRating = value => ({ type: types.SET_RATING, value });
const resetSearchPanel = () => ({ type: types.RESET_SEARCH_PANEL });

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

export const fetchTours = () => async (dispatch, getState) => {
  const {
    searchValue: search,
    isSorted,
    isSpecialOffer,
    currentPrice,
    currentRating
  } = getState().searchPage;
  const params = {
    search,
    isSorted,
    isSpecialOffer,
    currentPrice,
    currentRating
  };
  dispatch(startLoading());
  try {
    // just for loader and emulate BE request :)
    await sleep(500);
    // should be like that
    // const { tours } = await axios.get('/tours', {params});
    const { tours } = await Promise.resolve(getFakeTours(params));
    dispatch(addTours(tours));
  } catch (error) {
    toastr.error(error.message || 'Error');
  } finally {
    dispatch(stopLoading());
  }
};

export const onSpecialOfferClick = () => dispatch => {
  dispatch(setSpecialOffer());
  dispatch(fetchTours());
};

export const onSearchChange = value => dispatch => {
  dispatch(setSearch(value));
  dispatch(fetchTours());
};

export const onSortClick = () => dispatch => {
  dispatch(setSort());
  dispatch(fetchTours());
};

export const onPriceChange = value => dispatch => {
  dispatch(setPrice(value));
  dispatch(fetchTours());
};

export const onRatingChange = value => dispatch => {
  dispatch(setRating(value));
  dispatch(fetchTours());
};

export const onClearAll = () => dispatch => {
  dispatch(resetSearchPanel());
  dispatch(fetchTours());
};
