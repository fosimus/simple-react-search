import React from 'react';
import { NavLink as Link } from 'react-router-dom';
import './styles.less';

const Navigation = () => (
  <div className="nav">
    <div className="nav_wrapper">
      <Link activeClassName="active" exact className="nav_link" to="/">
        Home
      </Link>
      <Link activeClassName="active" exact className="nav_link" to="/search">
        Search
      </Link>
    </div>
  </div>
);

export default Navigation;
