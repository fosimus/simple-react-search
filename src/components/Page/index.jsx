import React from 'react';
import PropTypes from 'prop-types';
import './styles.less';

const Page = ({ title }) => (
  <div className="page">
    <div className="page_title">
      <h1 className="title">{title}</h1>
    </div>
  </div>
);

Page.propTypes = {
  title: PropTypes.string
};

Page.defaultProps = {
  title: ''
};

export default Page;
