import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

class Price extends PureComponent {
  onPriceChange = e => {
    this.props.onPriceChange(e.target.value);
  };

  render() {
    const { disabled, currentPrice } = this.props;
    return (
      <select
        className={`control control-select ${currentPrice ? 'active' : ''}`}
        value={currentPrice}
        onChange={this.onPriceChange}
        disabled={disabled}
      >
        <option value="">Price</option>
        <option value="0:10">0 - 10</option>
        <option value="10:50">10 - 50</option>
        <option value="50:100">50 - 100</option>
        <option value="100:300">100 - 300</option>
      </select>
    );
  }
}

Price.propTypes = {
  currentPrice: PropTypes.string,
  disabled: PropTypes.bool,
  onPriceChange: PropTypes.func.isRequired
};

Price.defaultProps = {
  currentPrice: '',
  disabled: false
};

export default Price;
