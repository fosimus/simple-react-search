import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import './styles.less';

const Star = ({ count, currentRating, onRatingChange }) => (
  <span
    className={`star ${currentRating >= count ? 'star-full' : ''}`}
    data-rating={count}
    onClick={onRatingChange}
    role="button"
    tabIndex="0"
  >
    ☆
  </span>
);

Star.propTypes = {
  count: PropTypes.number.isRequired,
  currentRating: PropTypes.string,
  onRatingChange: PropTypes.func.isRequired
};

Star.defaultProps = {
  currentRating: ''
};

class Rating extends PureComponent {
  onRatingChange = ({ target: { dataset: { rating } } }) => {
    const { disabled } = this.props;
    if (!disabled) {
      this.props.onRatingChange(rating);
    }
  };

  render() {
    const { currentRating } = this.props;
    return (
      <span className="rating">
        {[5, 4, 3, 2, 1].map(count => (
          <Star
            key={count}
            count={count}
            currentRating={currentRating}
            onRatingChange={this.onRatingChange}
          />
        ))}
      </span>
    );
  }
}

Rating.propTypes = {
  currentRating: PropTypes.string,
  disabled: PropTypes.bool,
  onRatingChange: PropTypes.func.isRequired
};

Rating.defaultProps = {
  currentRating: '',
  disabled: false
};

export default Rating;
