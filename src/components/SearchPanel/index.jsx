import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash.debounce';
import Rating from '../Rating';
import Price from '../Price';
import './styles.less';

class SearchPanel extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      searchValue: ''
    };
    // good practice to use debounce when user is typing
    this.onSearchChange = debounce(this.props.onSearchChange, 500);
    this.textInput = React.createRef();
  }

  componentDidMount() {
    this.textInput.current.focus();
  }

  componentDidUpdate() {
    this.textInput.current.focus();
  }

  onInputSearchChange = ({ target: { value: searchValue } }) => {
    this.setState({ searchValue }, () => {
      this.onSearchChange(searchValue);
    });
  };

  render() {
    const {
      currentPrice,
      currentRating,
      isLoading,
      isSorted,
      isSpecialOffer,
      onClearAll,
      onPriceChange,
      onRatingChange,
      onSortClick,
      onSpecialOfferClick
    } = this.props;
    return (
      <div
        className={`search-panel ${isLoading ? 'search-panel-disabled' : ''}`}
      >
        <input
          ref={this.textInput}
          className="control control_search"
          type="input"
          placeholder="Type something to start searching..."
          value={this.state.searchValue}
          onChange={this.onInputSearchChange}
          disabled={isLoading}
        />
        <input
          className={`control ${isSorted ? 'active' : ''}`}
          type="button"
          value="Sort by title"
          onClick={onSortClick}
          disabled={isLoading}
        />
        <input
          className={`control ${isSpecialOffer ? 'active' : ''}`}
          type="button"
          value="Special offers"
          onClick={onSpecialOfferClick}
          disabled={isLoading}
        />
        <Price
          currentPrice={currentPrice}
          onPriceChange={onPriceChange}
          disabled={isLoading}
        />
        <Rating
          currentRating={currentRating}
          onRatingChange={onRatingChange}
          disabled={isLoading}
        />
        <input
          className="control control-clear"
          type="button"
          value="Clear all"
          onClick={onClearAll}
          disabled={isLoading}
        />
      </div>
    );
  }
}

SearchPanel.propTypes = {
  currentPrice: PropTypes.string,
  currentRating: PropTypes.string,
  isLoading: PropTypes.bool,
  isSorted: PropTypes.bool,
  isSpecialOffer: PropTypes.bool,
  onClearAll: PropTypes.func.isRequired,
  onPriceChange: PropTypes.func.isRequired,
  onRatingChange: PropTypes.func.isRequired,
  onSearchChange: PropTypes.func.isRequired,
  onSortClick: PropTypes.func.isRequired,
  onSpecialOfferClick: PropTypes.func.isRequired
};

SearchPanel.defaultProps = {
  currentPrice: '',
  currentRating: '',
  isLoading: false,
  isSorted: false,
  isSpecialOffer: false
};

export default SearchPanel;
