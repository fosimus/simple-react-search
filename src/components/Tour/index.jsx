import React from 'react';
import PropTypes from 'prop-types';
import './styles.less';

const Tour = ({ title, price, currency, rating, isSpecialOffer, image }) => (
  <div
    className="tour"
    role="button"
    tabIndex="0"
    // temp alert, should be action to open new page or popup
    onClick={() => alert('New page will be here soon...')}
  >
    {isSpecialOffer && (
      <span className="tour_special-offer">Special Offer!</span>
    )}
    <div className="tour_rating">{rating}</div>
    <div className="tour_image">
      <img width="248" height="145" alt="" src={image} />
    </div>
    <div className="tour_title">{title}</div>
    <div className="tour_price">{`${currency} ${price}`}</div>
  </div>
);

Tour.propTypes = {
  currency: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  isSpecialOffer: PropTypes.bool.isRequired,
  price: PropTypes.string.isRequired,
  rating: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
};

export default Tour;
