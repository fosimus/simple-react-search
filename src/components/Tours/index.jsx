import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Tour from '../../components/Tour';
import Loading from '../../components/Loading';
import './styles.less';

const TourList = tours => {
  const isEmpty = tours.length === 0;
  return isEmpty ? (
    <div className="no-elements">No tours</div>
  ) : (
    <div className="tours">
      {tours.map(tour => (
        <div className="tours_item" key={tour.title}>
          <Tour {...tour} />
        </div>
      ))}
    </div>
  );
};

class Tours extends Component {
  componentDidMount() {
    this.props.onFetchTours();
  }

  render() {
    const { isLoading, tours } = this.props;
    return isLoading ? <Loading /> : TourList(tours);
  }
}

Tours.propTypes = {
  isLoading: PropTypes.bool,
  onFetchTours: PropTypes.func.isRequired,
  tours: PropTypes.array
};

Tours.defaultProps = {
  isLoading: false,
  tours: []
};

export default Tours;
