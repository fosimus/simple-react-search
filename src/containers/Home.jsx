import React from 'react';

const Home = () => (
  <div>
    Features:
    <ul>
      <li>
        <strong>Search by title</strong>: user can type something and app will
        find propper tours. The feature implemented with debounce, because it
        helps reduce many requests.
      </li>
      <li>
        <strong>Sort by title</strong>: helps to sort tours in case if user
        knows exactly city or other information.
      </li>
      <li>
        <strong>Sort by special offers</strong>: helps to find tours with
        promotions.
      </li>
      <li>
        <strong>Sort by price</strong>: helps to find tours with range of price.
      </li>
      <li>
        <strong>Sort by rating</strong>: helps to sort tours by rating.
      </li>
      <li>
        <strong>Clear all</strong>: helps to reset search panel data.
      </li>
      <li>
        <strong>Routing</strong> for different pages. In the current task there
        are two pages: Main and Search
      </li>
      <li>
        I also added images for tours because I belive everything what you do
        must be nice and beautiful, especially for FrontEnd development.
      </li>
    </ul>
    Technical scope:
    <ul className="ul">
      <li>
        <strong>React</strong> - because it is simple and powerful JavaScript
        lib for building user interfaces.
      </li>
      <li>
        <strong>Redux</strong> - because it helps to manage ever-changing state
        easily.
      </li>
      <li>
        <strong>React-Router</strong> - because it helps handle Sing Page
        Applications.
      </li>
      <li>
        <strong>Webpack</strong> - because it is a perfect bundler for many
        things :)
      </li>
      <li>
        <strong>Flexbox</strong> for views - because it is a good approach for
        creating page layouts.
      </li>
      <li>
        <strong>Eslint</strong> with airbnb rules - because all devs should
        follow the same rules!
      </li>
      <li>
        <strong>Components</strong> - because it is more maintainable and other
        devs easily can continue work on it.
      </li>
    </ul>
    This task is quite simple and can be implemented with Vanilla JS but I
    decided to show my current experience with new tech stack. For sure I can
    work with Vanilla JS check it out here:
    <ul>
      <li>
        <a
          className="link"
          href="https://gitlab.com/fosimus/tournament"
          target="_blank"
          rel="noopener noreferrer"
        >
          Vanilla JS, tournament game, requests optimization
        </a>
      </li>
      <li>
        <a
          className="link"
          href="https://gitlab.com/fosimus/shapes"
          target="_blank"
          rel="noopener noreferrer"
        >
          Vanilla JS + OOP approach, shape panel
        </a>
      </li>
      <li>
        <a
          className="link"
          href="https://k2yvrx4xz5.codesandbox.io/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Real sandbox
        </a>{' '}
        or{' '}
        <a
          className="link"
          href="https://gitlab.com/fosimus/Marvel-api"
          target="_blank"
          rel="noopener noreferrer"
        >
          React, Redux, Routing, Pagination, Marvel API
        </a>
      </li>
    </ul>
    if you want to see my face 😄 👉{' '}
    <a
      className="link"
      href="https://www.instagram.com/fos_andy/"
      target="_blank"
      rel="noopener noreferrer"
    >
      Instagram
    </a>{' '}
    <a
      className="link"
      href="https://www.facebook.com/ushakov.andrei"
      target="_blank"
      rel="noopener noreferrer"
    >
      Facebook
    </a>{' '}
    <a
      className="link"
      href="https://www.linkedin.com/in/ushakovandrey/"
      target="_blank"
      rel="noopener noreferrer"
    >
      Linkedin
    </a>
  </div>
);

export default Home;
