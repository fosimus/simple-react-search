import React, { Fragment } from 'react';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as allActions from '../actions';
import Tours from '../components/Tours';
import SearchPanel from '../components/SearchPanel';

const SearchPage = props => {
  const {
    actions,
    currentPrice,
    currentRating,
    isLoading,
    isSorted,
    isSpecialOffer,
    tours
  } = props;
  return (
    <Fragment>
      <h1 className="title">Find tickets around the world!</h1>
      <SearchPanel
        currentPrice={currentPrice}
        currentRating={currentRating}
        isLoading={isLoading}
        isSorted={isSorted}
        isSpecialOffer={isSpecialOffer}
        onClearAll={actions.onClearAll}
        onPriceChange={actions.onPriceChange}
        onRatingChange={actions.onRatingChange}
        onSearchChange={actions.onSearchChange}
        onSelectChange={actions.onUserNameChange}
        onSortClick={actions.onSortClick}
        onSpecialOfferClick={actions.onSpecialOfferClick}
      />
      <Tours
        isLoading={isLoading}
        tours={tours}
        onFetchTours={actions.fetchTours}
      />
    </Fragment>
  );
};

SearchPage.propTypes = {
  actions: PropTypes.object.isRequired,
  currentPrice: PropTypes.string.isRequired,
  currentRating: PropTypes.string.isRequired,
  isLoading: PropTypes.bool,
  isSorted: PropTypes.bool.isRequired,
  isSpecialOffer: PropTypes.bool.isRequired,
  tours: PropTypes.array
};

SearchPage.defaultProps = {
  isLoading: false,
  tours: []
};

const mapStateToProps = state => {
  const {
    currentPrice,
    currentRating,
    isLoading,
    isSorted,
    isSpecialOffer,
    searchValue,
    tours
  } = state.searchPage;

  return {
    currentPrice,
    currentRating,
    isLoading,
    isSorted,
    isSpecialOffer,
    searchValue,
    tours
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(allActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchPage);
