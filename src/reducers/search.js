import * as types from '../constants/search';

const initialState = {
  currentPrice: '',
  currentRating: '',
  isLoading: false,
  isSorted: false,
  isSpecialOffer: false,
  searchValue: '',
  tours: []
};

export default function posts(state = initialState, action) {
  switch (action.type) {
    case types.ADD_TOURS:
      return {
        ...state,
        tours: action.tours
      };
    case types.START_LOADING:
      return {
        ...state,
        isLoading: true
      };
    case types.STOP_LOADING:
      return {
        ...state,
        isLoading: false
      };
    case types.SET_SORTING:
      return {
        ...state,
        isSorted: !state.isSorted
      };
    case types.SET_SPECIAL_OFFER:
      return {
        ...state,
        isSpecialOffer: !state.isSpecialOffer
      };
    case types.SET_SEARCH:
      return {
        ...state,
        searchValue: action.value
      };
    case types.SET_PRICE:
      return {
        ...state,
        currentPrice: action.value
      };
    case types.SET_RATING:
      return {
        ...state,
        currentRating: action.value
      };
    case types.RESET_SEARCH_PANEL:
      return {
        ...state,
        currentPrice: '',
        currentRating: '',
        isSorted: false,
        isSpecialOffer: false,
        searchValue: ''
      };

    default:
      return state;
  }
}
